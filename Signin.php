<?php
include './header.php';
include './navigation.php';
?>
<html>
    <head>
        <title>Signin Form</title>
<!--        <link rel="stylesheet" href="css/bootstrap-theme.css">
        <link rel="stylesheet" href="css/bootstrap.css">
-->        <link rel="stylesheet" href="css/style.css">
        
    </head>
    <body>
        <br>
        <div class="container content_area">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="panel panel-primary" align="center">
                        <div class="panel-heading">Signin Form</div>
                        <form action="" method="post">
                            <table class="table" align="center">
                                    <div class="form-group">
                                        <tr>
                                            <td> <label for="username">User Name</label></td>
                                            <td> <input type="text" name="username"class="form-control col-md-6" id="username" placeholder="username" required></td>
                                        </tr>
                                    </div>
                                    <div class="form-group">
                                        <tr><td><label for="password">Password</label></td>
                                            <td><input type="password" name="password" class="form-control" id="password" placeholder="Password" required></td>
                                        </tr>
                                    </div>
                                    <td colspan="2" align="center"><button type="submit" class="btn btn-default">Submit</button></td>
                                    
                            </table>
                        </form>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
        <div class="footer_area">
            <?php include './footer.php';?>
        </div>
    </body>
</html>
