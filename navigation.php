<html>
    <head>
         <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <style type="text/css">
               .btn-social {
            color: white;
            opacity: 0.8;
        }

            .btn-social:hover {
                color: white;
                opacity: 1;
                text-decoration: none;
            }

        .btn-facebook {
            background-color: #3b5998;
        }

        .btn-twitter {
            background-color: #00aced;
        }

        .btn-linkedin {
            background-color: #0e76a8;
        }

        .btn-google {
            background-color: #c32f10;
        }
    </style>
    </head>
    <body>
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--<a class="navbar-brand" href="#">Design Bootstrap</a>-->
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right ">
                        <li role="presentation"><a href="index.php">Home</a></li>
                        <li role="presentation"><a href="Signin.php">Signin</a></li>
                        <li role="presentation"><a href="SignUp.php">Sign Up</a></li>
                        <li role="presentation"><a href="SignOut.php">Sign Out</a></li>
            </div>
        </div>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery-1.12.3.min.js"></script>
    </body>
</html>