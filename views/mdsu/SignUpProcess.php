<?php

include_once '../../vendor/autoload.php';
use App\MDSU\CMS;
session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
$obj=new CMS();
$obj->prepare($_POST);
$obj->SignUp();